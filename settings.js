settings = {
  wikiUrl: "http://en.wikipedia.org",
  refreshInterval: 2, //minutes
  watchlistSize: 5,
  longTitles: 'wrap',

  save: function() {
    var marshalled = JSON.stringify(this)
    localStorage.setItem('settings', marshalled)
    chrome.runtime.reload()
  },

  load: function() {
    var marshalled = localStorage.getItem('settings')
    var parsed = JSON.parse(marshalled)
    for (key in parsed) {
      if (parsed.hasOwnProperty(key)) {
        this[key] = parsed[key]
      }
    }
  }
}
