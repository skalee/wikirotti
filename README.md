This extension is for Opera 16 and above.  Check out [presto](https://bitbucket.org/skalee/wikirotti/src/presto) branch for version working with Opera 12 and older.

FEATURES:
=========

* Displaying watchlist for any Wikimedia Foundation wiki (and with some additional work for any MediaWiki site)
* Page title and time stamp are displayed
* 'You have new message' notification

TODO:
=====

* 'Some revisions wait for flagging' notification
* Customizing watchlist display: author, comment etc.
* Filtering out bot editions
* Distinguishing most recent (today's) editions from others
* Distinguishing editions not visited yet from others

USING WITH ANY MEDIAWIKI SITE:
==============================

Although this extension is designed to work with Wikimedia Foundation projects, you can easily modify it to work with arbitrary MediaWiki powered site.  In order to do it:

1. Download [the ZIP archive containing source code](https://bitbucket.org/skalee/wikirotti/get/master.zip) end extract it to separate directory.
1. Open `manifest.json` with your favourite text editor.
1. Do you see `permissions` and bunch of domains below it?  Add yet another one with the address of your wiki.
1. Type into browser's address bar `opera:extensions` and hit enter to navigate there.
1. Enable Developer Mode by clicking the button with such caption.
1. Click on the ’Load Unpacked Extension…’ and select the directory with modified extension.
1. Enjoy!

CONTRIBUTING:
=============

Yes, it could be prettier.  And it could have more features.  If you want to help, fork me on BitBucket!  If you don't know what does it mean and still want to help, [send me the message on BitBucket](https://bitbucket.org/account/notifications/send/?receiver=skalee).

Translations are most welcome and easy to do!  Current ones are located in [this directory](https://bitbucket.org/skalee/wikirotti/src/master/_locales).  In order to send me translations, either make fork or [message me](https://bitbucket.org/account/notifications/send/?receiver=skalee).

BUGS AND FEATURE REQUESTS:
==========================

If you notice a bug, please inform me about it on [this page](https://bitbucket.org/skalee/wikirotti/issues).  Please write what does not work (like should be X but is Y) and write few words about your configuration including: Opera version, operating system, language version of Wikirotti, site you try to connect.

Use the same page if you want to propose new feature.

LICENSING:
==========

Code base is dual-licensed on [MIT](http://www.opensource.org/licenses/MIT) and [Apache License 2.0](http://www.opensource.org/licenses/Apache-2.0).  Some pictures were not created by me and they are licensed differently.

Wikipedia logo is a registered trademark of Wikimedia Foundation.  It is taken from [here](https://commons.wikimedia.org/wiki/File:Wikipedia-logo.svg) and used according to terms of use published [here](https://wikimediafoundation.org/wiki/Trademark_Policy).

Mail icon is from Tango! Desktop Project, later modified by Innerer Schweinehund. It comes from [here](https://commons.wikimedia.org/wiki/File:Mail-closed.svg). [CC-BY-SA 2.5](https://creativecommons.org/licenses/by-sa/2.5/deed).
