window.addEventListener('load', function() {

  var form = document.getElementById('form_options')
  var i_url = document.getElementById('i_url')
  var i_interval = document.getElementById('i_interval')
  var i_size = document.getElementById('i_size')
  var i_hide_minor = document.getElementById('i_hide_minor')
  var i_hide_bots = document.getElementById('i_hide_bots')
  var i_hide_external = document.getElementById('i_hide_external')
  var i_long_wrap = document.getElementById('i_long_wrap')
  var i_long_shorten = document.getElementById('i_long_shorten')

  var init = function() {
    settingsToFormFields()
    form.addEventListener('submit', onSubmit)
  }

  var onSubmit = function() {
    formFieldsToSettings()
    window.close()
  }

  var settingsToFormFields = function() {
    settings.load()
    i_url.value = settings.wikiUrl
    i_interval.value = settings.refreshInterval
    i_size.value = settings.watchlistSize
    i_hide_minor.checked = settings.hideMinor
    i_hide_bots.checked = settings.hideBots
    i_hide_external.checked = settings.hideExternal
    if (settings.longTitles === 'shorten') {
      i_long_shorten.checked = true
    } else {
      i_long_wrap.checked = true
    }
  }

  var formFieldsToSettings = function() {
    settings.wikiUrl = i_url.value
    settings.refreshInterval = parseInt(i_interval.value, 10)
    settings.watchlistSize = parseInt(i_size.value, 10)
    settings.hideMinor = i_hide_minor.checked
    settings.hideBots = i_hide_bots.checked
    settings.hideExternal = i_hide_external.checked
    settings.longTitles = i_long_shorten.checked ? 'shorten' : 'wrap'
    settings.save()
  }

  init()

})
