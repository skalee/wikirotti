window.addEventListener('load', function() {

  var api_path = "/w/api.php"
  var eWatchlist = document.getElementById('watchlist')
  var eNewMessage = document.getElementById('new_message')
  var eNotLoggedIn = document.getElementById('not_logged_in')

  var init = function() {
    settings.load()
    setInterval(refresh, settings.refreshInterval * 1000 * 60)
    refresh()
  }

  var refresh = function() {
    var watchlistParams = {
      list: 'watchlist',
      wllimit: (settings.watchlistSize),
      wlprop: 'title|user|timestamp',
      wlshow: wlShowSettings(),
      wltype: 'new|edit|log' + (settings.hideExternal ? '' : '|external')
    }
    query(watchlistParams, onGetWatchlist)

    var checkForMessagesParams = {
      meta: 'userinfo',
      uiprop: 'hasmsg'
    }
    query(checkForMessagesParams, onCheckForMessages)
  }

  var onGetWatchlist = function(resp) {
    if (assertLoggedIn(resp)) {
      var htmls = resp.query.watchlist.map(function(item){
        return (
          '<li><span class="timestamp">' +
          prettyDate(item.timestamp) +
          '</span><span class="art">' +
          prettyTitle(item.title) +
          '</span></li>'
        )
      })

      if (htmls.length === 0) {
        htmls.push(
          '<li class="empty">' +
          chrome.i18n.getMessage("speeddial_empty_watchlist") +
          '</li>'
        )
      }

      eWatchlist.innerHTML = htmls.join('')
      eWatchlist.style.display = 'block'
      eWatchlist.className = settings.longTitles
    }
    position()
  }

  var onCheckForMessages = function(resp) {
    if (assertLoggedIn(resp)) {
      var hasMessages = (
        resp.query
        &&
        resp.query.userinfo
        &&
        resp.query.userinfo.messages !== undefined
      )

      if (hasMessages) {
        displayNewMessages()
      } else {
        hideNewMessages()
      }
    }
    position()
  }

  var assertLoggedIn = function(resp) {
    var notLoggedIn = (
      (resp.error && resp.error.code === 'wlnotloggedin')
      ||
      (resp.query && resp.query.userinfo && resp.query.userinfo.anon)
    )

    if (notLoggedIn) {
      displayNotLoggedIn()
    } else {
      hideNotLoggedIn()
    }

    return !notLoggedIn
  }

  var displayNotLoggedIn = function() {
    eNotLoggedIn.getElementsByTagName('span')[0].innerText = settings.wikiUrl
    eWatchlist.style.display = 'none'
    eNewMessage.style.display = 'none'
    eNotLoggedIn.style.display = 'block'

    var loginUrl = settings.wikiUrl + '/wiki/Special:UserLogin'
    opr.speeddial.update({url: loginUrl})
  }

  var hideNotLoggedIn = function() {
    eNotLoggedIn.style.display = 'none'

    var watchlistUrl = settings.wikiUrl + '/wiki/Special:Watchlist'
    opr.speeddial.update({url: watchlistUrl})
  }

  var displayNewMessages = function() {
    eNewMessage.style.display = 'block'
  }

  var hideNewMessages = function() {
    eNewMessage.style.display = 'none'
  }

  var prettyDate = function(timestamp) {
    var at = new Date(timestamp)
    var strs = ['getHours', 'getMinutes'].map(function(fun) {
      var str = (at[fun])().toString()
      return (str.length < 2 ? '0' + str : str)
    })

    return strs.join(':')
  }

  var prettyTitle = function(string) {
    if (settings.longTitles === 'shorten') {
      return string
    } else {
      return string.replace(/:/g, ':<wbr>').replace(/\//g, '/<wbr>')
    }
  }

  var position = function() {
    var margin = (window.innerHeight - (eWatchlist.offsetHeight || 0)) / 2 - (eNewMessage.offsetHeight || 0)
    eWatchlist.style.marginTop = (margin > 0 ? margin : 0) + 'px'
  }

  var wlShowSettings = function() {
    var result = []
    if (settings.hideBots) {
      result.push('!bots')
    }
    if (settings.hideMinor) {
      result.push('!minor')
    }
    return result.join('|')
  }

  var query = function(params, cb, err) {
    params.action = params.action || 'query'
    params.format = params.format || 'json'

    var url = settings.wikiUrl + api_path + '?'
    for (key in params) {
      if (params.hasOwnProperty(key)) {
        url = url + key + '=' + params[key] + '&'
      }
    }
    url = url.slice(0, -1)

    var xhr = new XMLHttpRequest()
    xhr.onreadystatechange = function() {
      if (xhr.readyState === 4 && xhr.status === 200) {
        cb(JSON.parse(xhr.responseText))
      }
    }

    xhr.open("GET", url)
    xhr.send()
  }

  init()

})
