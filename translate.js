window.addEventListener('load', function() {

  var translatableElements = document.querySelectorAll("*[data-t]")
  var element, i

  for (i = 0; i < translatableElements.length; ++i) {
    element = translatableElements[i]
    element.innerHTML = chrome.i18n.getMessage(element.dataset.t)
  }

})
